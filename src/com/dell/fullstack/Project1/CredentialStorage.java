package com.dell.fullstack.Project1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class CredentialStorage {

	public static String filepath = "credential.txt";
	boolean flagUsr,flagSite ,duplFlag ,lstFlag= false;
	String username , password;
	String site,user,pwd;
	public void save(String loggedUser){

		System.out.println("Enter the Site");
		Scanner sc = new Scanner(System.in);
		String plat = "Form"+sc.next();

		duplicatePlat(loggedUser, plat);

		if(duplFlag==false){
			System.out.println("Enter UserName");
			Scanner sc1 = new Scanner(System.in);
			user = sc1.next();


			System.out.println("Enter Password");
			Scanner sc2 = new Scanner(System.in);
			pwd = sc2.next();
			site = loggedUser+"--"+plat+"--"+user+"--"+pwd;

			RegistrationPage r = new RegistrationPage();
			r.writeFile(filepath,site);
			System.out.println(site);

			UserInput in   = new UserInput();
			in.continueSave(loggedUser);

		}
		else{


			UserInput in   = new UserInput();
			in.continueSave(loggedUser);

		}



	}

	public void retrieve(String retriveUser){

		System.out.println("Enter the Site");
		Scanner sc = new Scanner(System.in);
		String platf = "Form"+sc.next();
		//System.out.println(platf);
		readData(retriveUser,platf);
		if(lstFlag==false){

			System.out.println("No such site details");
		}

	}


	public void readData(String loggedUser,String plat){

		try {
			//System.out.println("entered read data");

			File file = new File("credential.txt");
			FileReader fw = new FileReader(file);
			BufferedReader buffer = new BufferedReader(fw);  
			String value;
			String[] contents = null;

			while( (value=buffer.readLine())!=null)

			{
				contents =value.split("--");
				//System.out.println(Arrays.toString(contents));
				flagUsr = contents[0].equals(loggedUser);
				flagSite =	contents[1].equals(plat);
				//System.out.println(flagUsr+"  "+flagSite);
				if(flagUsr==true && flagSite==true){

					String username =contents[2].toString();
					String password =contents[3].toString();

					System.out.println("Credentials Below");
					System.out.println("USERNAME:    "+  username);
					System.out.println("PASSWORD:     "+ password);

					lstFlag =true;
					break;

				}



			}

			buffer.close();

		}


		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}	

	public void duplicatePlat(String loggedUser,String plat){

		try {
			System.out.println("entered read data");

			File file = new File("credential.txt");
			FileReader fw = new FileReader(file);
			BufferedReader buffer = new BufferedReader(fw);  
			String value;
			String[] contents = null;

			while( (value=buffer.readLine())!=null)

			{
				contents =value.split("--");
				//System.out.println(Arrays.toString(contents));
				flagUsr = contents[0].equals(loggedUser);
				flagSite =	contents[1].equals(plat);
				//System.out.println(flagUsr+"  "+flagSite);
				if(flagUsr==true && flagSite==true){



					System.out.println("Credentials already Stored , No duplicates allowed");
					duplFlag= true;

					break;

				}
				else
					duplFlag=false;

			}

			buffer.close();

		}


		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}	


}
